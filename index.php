<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
  $dat = $_SESSION['datos_usuario'];
  if($dat['cargo']==="MASTER"){
    header('location: ./php/master/master.php');
  }
  else{
    if($dat['cargo']==="ADMINISTRATIVO"){
      header('location: ./php/admin/admin.php');
    }
    else{
      if($dat['cargo']==="PILOTO"){
        header('location: ./php/personal/personal.php');
      }
      else{
        if($dat['cargo']==="ASISTENTE DE VUELO"){
          header('location: ./php/personal/personal.php');
        }
      }
    }
  }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>My fly</title>
</head>
<body style="background-image: url('./img/fondo.png');">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="./index.php">
            <img src="./img/avion.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            FlyPlane
          </a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="./index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./php/login.php">Iniciar sesión</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Información</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca de</a>
            </li>
          </ul>
        </div>
        <span class="navbar-text" id="nombre">
            <?php
                if(isset($_SESSION['datos_usuario'])){
                    $dat_usr = $_SESSION['datos_usuario'];
                    $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                    echo "<a href='perfil.php'>$nom_nav<a>";
                }
                else{
                    echo "Bienvenido";
                }
            ?>
        </span>
      </nav>
      <div class="container">
        <div class="card mb-3 text-dark bg-warning">
          <img src="./img/carrusel_1.jpg" height="440" class="card-img-top" alt="despegar">
          <div class="card-body">
              <h5 class="card-title">Estamos juntos en todo destino!</h5>
              <p class="card-text">Somos una familia de tripulantes aéreos que trabajamos con calidad y alegría, estamos siempre dispuestos a brindar el mejor servicio, pero más aún a cuidarnos entre nosotros porque los pasajeros pasan, pero nosotros siempre estaremos juntos.</p><h4>FLYPLANE AIRLINES "UNA SOLA FAMILIA".</h4>
          </div>
      </div>
    </div>
    
</body>
</html>