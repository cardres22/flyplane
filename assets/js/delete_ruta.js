var formulario = document.getElementById('eliminar');

formulario.addEventListener('submit',function(e){
    e.preventDefault();
    console.log("diste click");
    var datos = new FormData(formulario);
    console.log(datos);
    console.log(datos.get('codigo'));
    fetch('../../php/admin/rutas/del_ruta.php',{
        method: 'POST',
        body: datos
    })
    .then(res=>res.text())
    .then(data=>{
        console.log(data);
        var respuesta = document.getElementById('alertar');
        respuesta.innerHTML = `<div class="alert alert-danger" role="alert">
            ${data}
        </div>`
    });


});