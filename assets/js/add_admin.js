var formulario = document.getElementById('formato');

formulario.addEventListener('submit',function(e){
    e.preventDefault();
    console.log("diste click");
    var datos = new FormData(formulario);
    fetch('../../php/master/proc_add.php',{
        method: 'POST',
        body: datos
    })
    .then(res=>res.text())
    .then(data=>{
        console.log(data);
        var respuesta = document.getElementById('alertar');
        respuesta.innerHTML = `<div class="alert alert-danger" role="alert">
            ${data}
        </div>`
    });

});