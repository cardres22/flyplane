
var formulario = document.getElementById('delete');

formulario.addEventListener('submit',function(e){
    e.preventDefault();
    console.log("diste click");
    var datos = new FormData(formulario);
    console.log(datos);
    console.log(datos.get('cedula'));
    fetch('../../php/master/proc_del.php',{
        method: 'POST',
        body: datos
    })
    .then(res=>res.text())
    .then(data=>{
        console.log(data);
        var respuesta = document.getElementById('alerta');
        respuesta.innerHTML = `<div class="alert alert-danger" role="alert">
            ${data}
        </div>`
    });

});