
<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="PILOTO"){
        if($dat['cargo']!="ASISTENTE DE VUELO"){
          header('location: ../../index.php');
        }
    }
}
else{
  header('location: ../../index.php');
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>My fly</title>
</head>
<body style="background-image: url('../../img/fondo.png');">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" id="avion" href="./personal.php">
            <img src="../../img/avion.svg" width="30" height="30" class="d-inline-block align-top" alt="avion" loading="lazy">
            FlyPlane
          </a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" id="home" href="./personal.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../logout.php">Cerrar sesión</a>
            </li>
          </ul>
        </div>
        <span class="navbar-text" id="nombre" >
            <?php
                if(isset($_SESSION['datos_usuario'])){
                    $dat_usr = $_SESSION['datos_usuario'];
                    $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                    echo "<a id='perfil'>$nom_nav<a>";
                }
                else{
                    echo "Bienvenido";
                }
            ?>
        </span>
      </nav>
      <div id="cuerpo">
      <div class="jumbotron">
          <?php
            if(isset($_SESSION['datos_usuario'])){
                $dat_usr = $_SESSION['datos_usuario'];
                $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                $num1 = "<h1 class='display-4'>HOLA, ".$nom_nav."</h1>";
                echo "$num1";
                $num2 = "<p class='lead'>Residente de la ciudad de ".$dat_usr['ciudad']."</p>";
                echo "$num2";
                echo "<hr class='my-4'>";
                $num4 = "<a class='btn btn-primary btn-lg' href='../logout.php' role='button'>cerrar sesión</a>";
                echo "$num4";
            }
            ?>
            
    </div>
    </div>
        <table class="table table-warning text-dark mt-2 mb-2">
        <thead>
        <tr>
          <th scope="col">Vuelo</th>
          <th scope="col">Fecha</th>
          <th scope="col">Origen</th>
          <th scope="col">Destino</th>
          <th scope="col">Piloto 1</th>
          <th scope="col">Piloto 2</th>
          <th scope="col">Asistente 1</th>
          <th scope="col">Asistente 2</th>
        </tr>
       </thead>
       <tbody>
            <?php
              require '../database.php';
              $consulta = 'SELECT cod_vue, fecha, origen,destino, nom_p1, nom_p2, nom_as1, nom_as2 FROM vuelos JOIN rutas ON ruta=codigo WHERE ced_p1 like :cedula OR ced_p2 like :cedula OR ced_as1 like :cedula OR ced_as2 like :cedula ORDER BY 2;';
              $select = $conn->prepare($consulta);
              $cedula = "".$dat['cedula'];
              $select->bindParam(':cedula',$cedula);
              $select->execute();
              $result = $select->fetchAll();
              foreach($result as $datos){
                $codigo = '<td>'.$datos['cod_vue'].'</td>';
                echo '<tr>';
                echo ($codigo);
                $nom = '<td>'.$datos['fecha'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['origen'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['destino'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['nom_p1'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['nom_p2'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['nom_as1'].'</td>';
                echo ($nom);
                $nom = '<td>'.$datos['nom_as2'].'</td>';
                echo ($nom);
                echo '</tr>';
             }
            ?>
          </tbody>
    <div>

    </div>
    
    <script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="../../assets/js/master.js"></script>
</body>
</html>