<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<div class="container" id="admin_vuelos">
<div class="card bg-info" style="width: 18rem; float:left" >
  <div class="card-body col-sm">
    <h5 class="card-title">Agregar vuelo</h5>
    <p class="card-text">Estamos listos para volar, en este espacio podras crear un nuevo vuelo, VAMOS A POR ELLO!.</p>
    <a id="agregar_vuelo"class="btn btn-success">Continuar</a>
  </div>
</div>
<div class="card col-sm bg-success" style="width: 18rem; float:right">
  <div class="card-body">
    <h5 class="card-title">Listar vuelos</h5>
    <p class="card-text">Aquí conocerás al personal de vuelo y la ruta, así que HORA DE VOLAR!.</p>
    <a id="listar" class="btn btn-primary">Continuar</a>
  </div>
</div>
</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/admin_vuelos.js"></script>