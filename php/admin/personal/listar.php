<?php
 session_start();
 require '../../database.php';
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../../index.php');
    }
    else{
        $consulta = 'SELECT nombre, apellido, ciudad, cargo, cedula FROM usuarios WHERE cargo like "PILOTO" OR cargo like "ASISTENTE DE VUELO" ORDER BY 5,1';
        $select = $conn->prepare($consulta);
        $select->execute();
        $result = $select->fetchAll();
    }
}
else{
  header('location: ../../../../index.php');
}
?>

<div class ="text-center container text-warning">
    <h1>TRIPULANTES DE LA EMPRESA</h1>
</div>
<table class="table table-warning text-dark mt-2">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Ciudad</th>
      <th scope="col">Cargo</th>
      <th scope="col">Cedula</th>
    </tr>
  </thead>
  <tbody>
    <?php
        foreach($result as $datos){
            $codigo = '<td>'.$datos['nombre'].'</td>';
            echo '<tr>';
            echo ($codigo);
            $nom = '<td>'.$datos['apellido'].'</td>';
            echo ($nom);
            echo '<td>'.$datos['ciudad'].'</td>';
            echo '<td>'.$datos['cargo'].'</td>';
            echo '<td>'.$datos['cedula'].'</td>';
            echo '</tr>';
        }
    ?>
  </tbody>
</table>
<form class="form-inline" id="eliminar">
  <div class="form-group mb-2">
    <label for="staticEmail2" class="sr-only">Email</label>
    <input type="text" readonly class="form-control-plaintext text-white" id="staticEmail2" value="personal a eliminar">
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" name="cedula" placeholder="Codigo de personal">
  </div>
  <button type="submit" class="btn btn-danger mb-2">Eliminar</button>
</form>

<div class="mt-2" id="alertar">

</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/delete_personal.js"></script>