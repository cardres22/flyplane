<?php
    session_start();
    if(!empty ($_POST['cedula']) && !empty ($_POST['password'])){
        if($_POST['password']!=$_POST['password_2']){
            echo("las contraseñas no son iguales.");
        }
        else{
            require '../../database.php';
            
            $insert = "INSERT INTO usuarios (nombre, apellido, ciudad, cargo, cedula, password) 
            VALUES (:nombre, :apellido, :ciudad, :cargo, :cedula, :pass)";
            $sts = $conn->prepare($insert);
            $sts->bindParam(':nombre',$_POST['nombre']);
            $sts->bindParam(':apellido',$_POST['apellido']);
            $sts->bindParam(':ciudad',$_POST['ciudad']);
            $sts->bindParam(':cargo',$_POST['cargo']);
            $cedula = ''.$_POST['cedula'];
            $sts->bindParam(':cedula',$cedula);
            $pass_enc = password_hash($_POST['password'],PASSWORD_BCRYPT);
            $sts->bindParam(':pass',$pass_enc);
            if($sts->execute()){
                echo  ("Usuario creado con exito \n Bienvenido ".$_POST['cargo'].": ".$_POST['nombre']." ".$_POST['apellido']);
                /*
                $con="C_".$_POST['cedula'];
                $result=$conn->query("CREATE TABLE IF NOT EXISTS ".$con." (
                codigo_vuelo VARCHAR(20) NOT NULL,
                origen VARCHAR(20) NOT NULL,
                destino VARCHAR(20) NOT NULL,
                fecha DATE NOT NULL,
                PRIMARY KEY (fecha)
            )"
            );*/
            }
            else{
                echo ("Ups! ocurrió algo inesperado, no se pudo crear el usuario"); 
            }
        }
    }
    //aviones123//
?>