<?php
 session_start();
 require '../../database.php';
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../../index.php');
    }
    else{
        $consulta = 'SELECT codigo,origen,destino FROM rutas ORDER BY 1';
        $select = $conn->prepare($consulta);
        $select->execute();
        $result = $select->fetchAll();
    }
}
else{
  header('location: ../../../../index.php');
}
?>
<div class ="text-center container text-warning">
    <h1>RUTAS DE VUELO</h1>
</div>
<table class="table table-warning text-dark mt-2">
  <thead>
    <tr>
      <th scope="col">Código</th>
      <th scope="col">Origen</th>
      <th scope="col">Destino</th>
    </tr>
  </thead>
  <tbody>
    <?php
    
        foreach($result as $datos){
            $codigo = '<td>'.$datos['codigo'].'</td>';
            echo '<tr>';
            echo ($codigo);
            $nom = '<td>'.$datos['origen'].'</td>';
            echo ($nom);
            echo '<td>'.$datos['destino'].'</td>';
            echo '</tr>';
        }
    ?>
  </tbody>
</table>
<form class="form-inline" id="eliminar">
  <div class="form-group mb-2">
    <label for="staticEmail2" class="sr-only">Email</label>
    <input type="text" readonly class="form-control-plaintext text-white" id="staticEmail2" value="Vuelo a eliminar">
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <input type="text" class="form-control" name="codigo" placeholder="Codigo de vuelo">
  </div>
  <button type="submit" class="btn btn-danger mb-2">Eliminar</button>
</form>

<div class="mt-2" id="alertar">

</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/delete_ruta.js"></script>