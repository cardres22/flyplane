<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<div class="text-white text-center">
    <h1>AGREGAR UNA RUTA</h1>
<form id="formato">
  <div class="row">
  <div class="col-4">
  <input type="text" class="form-control" placeholder="codigo" name="codigo">
  </div>
    <div class="col-4">
      <input type="text" class="form-control" placeholder="Ciudad origen" name="origen" style="text-transform: uppercase;">
    </div>
    <div class="col-4">
      <input type="text" class="form-control" placeholder="Ciudad destino" name="destino" style="text-transform: uppercase;">
    </div>
  </div>
  <button class="btn btn-primary mt-4" type="submit">Agregar</button>
</form>
</div>
<div class="mt-2" id="alertar">

</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/add_ruta.js"></script>