<?php
 session_start();
 require '../../database.php';
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../../index.php');
    }
    else{
        $consulta = 'SELECT nombre, apellido, ciudad, cargo, cedula FROM usuarios WHERE cargo like "PILOTO" OR cargo like "ASISTENTE DE VUELO" ORDER BY 5,1';
        $select = $conn->prepare($consulta);
        $select->execute();
        $result = $select->fetchAll();
    }
}
else{
  header('location: ../../../../index.php');
}
?>
<div class ="text-center container text-warning">
    <h1>AGREGAR UN VUELO</h1>
</div>
<form id="formato">
  <div class="row">
  <div class="col-4">
  <input type="text" class="form-control" placeholder="numero de vuelo" name="codigo">
  </div>
    <div class="col-4">
      <input type="number" class="form-control" placeholder="Codigo de ruta" name="ruta" style="text-transform: uppercase;">
    </div>
    <div class="col-4">
      <input type="date" class="form-control" placeholder="fecha" name="fecha" style="text-transform: uppercase;">
    </div>
  </div>
  <div class="row mt-2">
  <div class="col-3">
  <input type="number" class="form-control" placeholder="PILOTO 1" name="pi_1">
  </div>
    <div class="col-3">
      <input type="number" class="form-control" placeholder="PILOTO 2" name="pi_2" style="text-transform: uppercase;">
    </div>
    <div class="col-3">
      <input type="number" class="form-control" placeholder="ASISTENTE 1" name="as_1" style="text-transform: uppercase;">
    </div>
    <div class="col-3">
      <input type="number" class="form-control" placeholder="ASISTENTE 2" name="as_2" style="text-transform: uppercase;">
    </div>
  </div>
  <button class="btn btn-primary mt-4" type="submit">Agregar</button>
</form>
</div>
<div class="mt-2" id="alertar">

</div>

<div style="display:flex">
  

  <div >
  <div class ="text-center container text-warning">
      <h1>TRIPULANTES</h1>
  </div>
    <table class="table table-warning text-dark mt-2">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">Apellido</th>
          <th scope="col">Ciudad</th>
          <th scope="col">Cargo</th>
          <th scope="col">Cedula</th>
        </tr>
      </thead>
      <tbody>
        <?php
            foreach($result as $datos){
                $codigo = '<td>'.$datos['nombre'].'</td>';
                echo '<tr>';
                echo ($codigo);
                $nom = '<td>'.$datos['apellido'].'</td>';
                echo ($nom);
                echo '<td>'.$datos['ciudad'].'</td>';
                echo '<td>'.$datos['cargo'].'</td>';
                echo '<td>'.$datos['cedula'].'</td>';
                echo '</tr>';
            }
        ?>
      </tbody>
    </table>
  </div>
  <div class="container">
    <div class ="text-center container text-warning">
        <h1>RUTAS DE VUELO</h1>
    </div>
    <table class="table table-warning text-dark mt-2">
      <thead>
        <tr>
          <th scope="col">Código</th>
          <th scope="col">Origen</th>
          <th scope="col">Destino</th>
        </tr>
      </thead>
      <tbody>
        <?php
            $consulta = 'SELECT codigo,origen,destino FROM rutas ORDER BY 1';
            $select = $conn->prepare($consulta);
            $select->execute();
            $result = $select->fetchAll();
            foreach($result as $datos){
                $codigo = '<td>'.$datos['codigo'].'</td>';
                echo '<tr>';
                echo ($codigo);
                $nom = '<td>'.$datos['origen'].'</td>';
                echo ($nom);
                echo '<td>'.$datos['destino'].'</td>';
                echo '</tr>';
            }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/vuelo.js"></script>