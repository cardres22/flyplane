<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" id="label">Aerolinea</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" id="inicio">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" id="vuelos">Vuelos</a>
      <a class="nav-item nav-link" id="rutas">Rutas</a>
      <a class="nav-item nav-link" id="gente">Aministrar personal</a>
    </div>
  </div>
</nav>
<div class="container bg-dark mt-5" id="contenedor">
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">VAMOS A VOLAR!</h1>
      <p class="lead">En esta sección podremos administrar los vuelos y el personal de nuestra empresa, mucha suerte querido administrador y hora de prender motores.</p>
  </div>
</div>
<div>
<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/admin_aerolinea.js"></script>