<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<div class="container" id="admin_user">
<div class="card bg-info" style="width: 18rem; float:left" >
  <div class="card-body col-sm">
    <h5 class="card-title">Agregar usuarios</h5>
    <p class="card-text">En esta opción podrás agregar nuevos usuarios de tipo Piloto y Asistente de vuelo.</p>
    <a id="agregar_usuario"class="btn btn-success">Continuar</a>
  </div>
</div>
<div class="card col-sm bg-success" style="width: 18rem; float:right">
  <div class="card-body">
    <h5 class="card-title">Listar usuarios</h5>
    <p class="card-text">Deseas ver al personal? esta es la opción que necesitas, para acceder has click en el siguiente botón.</p>
    <a id="listar" class="btn btn-primary">Continuar</a>
  </div>
</div>
</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/admin_personal.js"></script>
