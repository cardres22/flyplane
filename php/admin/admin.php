<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="ADMINISTRATIVO"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>My fly</title>
</head>
<body style="background-image: url('../../img/fondo.png');">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" id="avion" href="./admin.php">
            <img src="../../img/avion.svg" width="30" height="30" class="d-inline-block align-top" alt="avion" loading="lazy">
            FlyPlane
          </a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" id="home" href="./admin.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../logout.php">Cerrar sesión</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"  id="personal">Personal</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="admin">Aerolinea</a>
            </li>
          </ul>
        </div>
        <span class="navbar-text" id="nombre" >
        <a  id="user" href="./admin.php">
            <?php
                if(isset($_SESSION['datos_usuario'])){
                    $dat_usr = $_SESSION['datos_usuario'];
                    $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                    echo "<a id='perfil'>$nom_nav<a>";
                }
                else{
                    echo "Bienvenido";
                }
            ?>
            </a>
        </span>
      </nav>
      <div id="cuerpo">
      <div class="jumbotron">
          <?php
            if(isset($_SESSION['datos_usuario'])){
                $dat_usr = $_SESSION['datos_usuario'];
                $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                $num1 = "<h1 class='display-4'>HOLA, ".$nom_nav."</h1>";
                echo "$num1";
                $num2 = "<p class='lead'>Residente de la ciudad de ".$dat_usr['ciudad']."</p>";
                echo "$num2";
                echo "<hr class='my-4'>";
                $num3 = "<p>Con cedula numero: ".$dat_usr['cedula'].". Usted es un administrador de la compañia, usted cuenta con el poder de ingresar nuevos pilotos o asistentes de vuelo, ademas puede ingresar nuevos tipos de vuelo y nuevos vuelos, buen viento y pon a volar a nuetro equipo.</p>";
                echo "$num3";
                $num4 = "<a class='btn btn-primary btn-lg' href='../logout.php' role='button'>cerrar sesión</a>";
                echo "$num4";
            }
            ?>
            
    </div>
       </div>
    
    <script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="../../assets/js/admin.js"></script>
</body>
</html>