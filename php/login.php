<?php
    session_start();
    $message='';
    if(!empty($_POST['cedula'])&& !empty($_POST['password'])){
        require './database.php';
        $consulta = 'SELECT nombre, apellido, ciudad, cargo, cedula, password FROM usuarios WHERE cedula like :cedula';
        $select = $conn->prepare($consulta);
        $select->bindParam(':cedula',$_POST['cedula']);
        $select->execute();
        $result = $select->fetch(PDO::FETCH_ASSOC);
        if($result != null){
            if(password_verify($_POST['password'],$result['password'])){
                $_SESSION['datos_usuario'] = $result;
                
                if($result['cargo'] === "MASTER"){
                    header('location: ./master/master.php');
                }
                else{
                    if($result['cargo']==="ADMINISTRATIVO"){
                    header('location: ./admin/admin.php');
                    }
                    else{
                        if($result['cargo']==="PILOTO" || $result['cargo']==="ASISTENTE DE VUELO"){
                        header('location: ./personal/personal.php');
                        }
                    }
                }
               
               
            }
            else{
                $message = 'La contraseña es incorrecta';
            }

        }
        else{
            $message ='El usuario no existe';
        }
    }

?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>My fly</title>
</head>
<body style="background-image: url('../img/fondo.png');background-repeat: no-repeat;background-position: 40% 0%;">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">
            <img src="../img/avion.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            FlyPlane
          </a>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="../index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./login.php">Iniciar sesión</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Información</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca de</a>
            </li>
          </ul>
        </div>
        <span class="navbar-text" id="nombre">
            <?php
                if(isset($_SESSION['datos_usuario'])){
                    $dat_usr = $_SESSION['datos_usuario'];
                    $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                    echo "<a href='../index.php'>$nom_nav<a>";
                }
                else{
                    echo "Bienvenido";
                }
            ?>
        </span>
  
    </nav>
    <div style="color:white;">
    <?php
        if(!empty($message)){
            $mensaje = '<script language="javascript">alert("'.$message.'");</script>';
            echo $mensaje;
        }
    ?>
    </div>
    <div style="margin-left:37%;margin-right:35%; margin-top: 30px">
        <div class="card" style="width: 18rem; text-align:center">
            <img src="../img/carrusel_2.jpg" class="card-img-top" alt="...">
            <div class="card-body">
            <form class="needs-validation" action="login.php" method="POST" name="login" enctype="multipart/form-data"novalidate>
                <div class="form-group">
                    <label for="exampleInputEmail1">Cédula</label>
                    <input type="number" class="form-control" name="cedula" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">Por favor digite su cedula.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password"id="exampleInputPassword1">
                </div>
                <?php
                if (isset($_SESSION['datos_usuario'])){
                    echo "<a href='../index.php'><button type='button' class='btn btn-primary'>Sesión iniciada</button></a>";
                    echo "<br></br>";
                    echo "<a href='logout.php'><button type='button' class='btn btn-primary'>Cerrar sesión</button></a>";
                }
                else{
                   echo "<button type='submit' class='btn btn-primary'>Ingresar</button>";
                }
                ?>
            
            </form>
        </div>
    </div>
    
</body>
</html>