<?php
    session_start();
    $alerta = '';
    if(!empty ($_POST['cedula']) && !empty ($_POST['password'])){
        if($_POST['password']!=$_POST['password_2']){
            $alerta="las contraseñas no son iguales.";
        }
        else{
            require './database.php';
            $insert = "INSERT INTO usuarios (nombre, apellido, ciudad, cargo, cedula, password) 
            VALUES (:nombre, :apellido, :ciudad, :cargo, :cedula, :pass)";
            $sts = $conn->prepare($insert);
            $sts->bindParam(':nombre',$_POST['nombre']);
            $sts->bindParam(':apellido',$_POST['apellido']);
            $sts->bindParam(':ciudad',$_POST['ciudad']);
            $sts->bindParam(':cargo',$_POST['cargo']);
            $cedula = ''.$_POST['cedula'];
            $sts->bindParam(':cedula',$cedula);
            $pass_enc = password_hash($_POST['password'],PASSWORD_BCRYPT);
            $sts->bindParam(':pass',$pass_enc);
            if($sts->execute()){
                $alerta = "Usuario creado con exito \n Bienvenido ".$_POST['cargo'].": ".$_POST['nombre']." ".$_POST['apellido'];
            }
            else{
                $alerta = "Ups! ocurrió algo inesperado, no se pudo crear el usuario"; 
            }
        }
    }
    //ingenieromaster//
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>My fly</title>
</head>
<body style="background-color: rgb(0, 70, 221);">

    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="./index.php">
            <img src="./img/avion.svg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            FlyPlane
          </a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="./index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./registrarse.php">Registrarse</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./login.php">Iniciar sesión</a>  
            </li>
          </ul>
        </div>
        <span class="navbar-text" id="nombre">
            <?php
                if(isset($_SESSION['datos_usuario'])){
                    $dat_usr = $_SESSION['datos_usuario'];
                    $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                    echo "<a href='perfil.php'>$nom_nav<a>";
                }
                else{
                    echo "Bienvenido";
                }
            ?>
        </span>
      </nav>
      <div style="margin-left:20%; margin-right:20%;margin-top:20px">
        <div class="card text-center" style="background-color:  rgba(238, 238,238,0.6);">
            <div class="card-header text-dark bg-success">
                BIENVENIDO A LA FAMILIA!
            </div>
            <div class="card-body">
                <h5 class="card-title ">Registra aquí tus datos</h5>
    <form class="needs-validation" action="registrarse.php" method="POST" name="registro" enctype="multipart/form-data"novalidate>
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom01">Nombres</label>
            <input type="text" class="form-control" name="nombre" id="validationCustom01" value="Marco" required>
            </div>
            <div class="col-md-6 mb-3">
            <label for="validationCustom02">Apellidos</label>
            <input type="text" class="form-control" name="apellido" id="validationCustom02" value="Polo" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom03">Ciudad</label>
            <input type="text" class="form-control" name="ciudad" id="validationCustom03" required>
            </div>
            <div class="col-md-3 mb-3">
            <label for="validationCustom04">Cargo</label>
            <select class="custom-select" name="cargo" id="validationCustom04" required>
                <option selected disabled value="">Selecciona</option>
                <option>PILOTO</option>
                <option>ASISTENTE DE VUELO</option>
                <option>ADMINISTRATIVO</option>
                <option>MASTER</option>
            </select>
            </div>
            <div class="col-md-3 mb-3">
            <label for="validationCustom05">Cédula</label>
            <input type="number" class="form-control" name="cedula" id="validationCustom05" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom01">Contraseña</label>
            <input type="password" class="form-control" name="password" id="validationCustom01" required>
            </div>
            <div class="col-md-6 mb-3">
            <label for="validationCustom02">Escriba de nuevo su contraseña</label>
            <input type="password" class="form-control" name="password_2" id="validationCustom02"  required>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Registrar</button>
    </form>
            </div>
            <div class="card-footer text-muted text-dark bg-warning">
            <?php
                if(!empty($alerta)){
                    echo "<h3>$alerta</h3>";
                }
                else{
                    echo "<h3>Vamos juntos por las alturas</h3>";
                }
            ?>
            </div>
        </div>
      </div>
</body>
</html>