<div class="jumbotron">
          <?php
            session_start();
            if(isset($_SESSION['datos_usuario'])){
                $dat_usr = $_SESSION['datos_usuario'];
                $nom_nav = $dat_usr['cargo'].": ".$dat_usr['nombre']." ".$dat_usr['apellido'];
                $num1 = "<h1 class='display-4'>HOLA, ".$nom_nav."</h1>";
                echo "$num1";
                $num2 = "<p class='lead'>Residente de la ciudad de ".$dat_usr['ciudad']."</p>";
                echo "$num2";
                echo "<hr class='my-4'>";
                $num3 = "<p>Con cedula numero: ".$dat_usr['cedula'].". Usted es el perfil super usuario de la compañia, cuenta con el poder de creear, modificar y eliminar perfiles administradores, crear nuevos perfiles master y ver la información de todos los usuarios de la compañia.</p>";
                echo "$num3";
                $num4 = "<a class='btn btn-primary btn-lg' href='../logout.php' role='button'>cerrar sesión</a>";
                echo "$num4";
            }
            ?>
            
    </div>