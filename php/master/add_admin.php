<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="MASTER"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<ul class="nav justify-content-end bg-light">
  <li class="nav-item">
    <a class="nav-link active" href="javascript: void(0)" id="agregar">Agregar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="javascript: void(0)" id="eliminar">Eliminar</a>
  </li>
</ul>
<div class="container bg-dark mt-5" id="contenedor">
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">ADMINISTRAR PERFILES</h1>
      <p class="lead">En esta sección podremos administrar usuarios master y administrativos, aqui podemos eliminarlos y crearlos.</p>
  </div>
</div>
<div>
<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/master_admin.js"></script>