<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="MASTER"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>
<div style="text-align:center">
    <div class="card bg-dark" >
    <div class="card-body bg-dark">
        <h2 class="bg-dark text-white">ELIMINAR UN ADMINISTRADOR</h2>
    </div>
        <div class="bg-dark">
            <div class="card" style="width: 18rem; margin-left:37%">
            <img src="../../img/carrusel_2.jpg" class="card-img-top" alt="...">
            <div class="card-body">
            <form class="needs-validation" id="delete">
                <div class="form-group">
                    <label for="exampleInputEmail1">Cédula</label>
                    <input type="number" class="form-control" name="cedula" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">Por favor digite la cedula del administrador.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password"id="exampleInputPassword1">
                    <small id="emailHelp" class="form-text text-muted">Por favor digite su contraseña de MASTER.</small>
                </div>
                <button id="enviar" type='submit' class='btn btn-primary'>Borrar</button>
            </form>
        </div>
        </div>
        <div class="mt-3" id="alerta">
            
        </div>
    </div>
</div>

<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="../../assets/js/delete_admin.js"></script>