<?php
 session_start();
 require '../database.php';
 require '../../fpdf/fpdf.php';
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="MASTER"){
        header('location: ../../index.php');
    }
    else{
        $consulta = 'SELECT nombre, apellido, ciudad, cargo, cedula FROM usuarios ORDER BY 4,1';
        $select = $conn->prepare($consulta);
        $select->execute();
        $result = $select->fetchAll();
    }
}
else{
  header('location: ../../index.php');
}
?>

<table class="table table-white text-white">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Ciudad</th>
      <th scope="col">Cargo</th>
      <th scope="col">Cedula</th>
    </tr>
  </thead>
  <tbody>
    <?php
        foreach($result as $datos){
            echo $datos['apellido'];
            echo '<tr>';
            $nom = '<td>'.$datos['nombre'].'</td>';
            echo ($nom);
            echo '<td>'.$datos['apellido'].'</td>';
            echo '<td>'.$datos['ciudad'].'</td>';
            echo '<td>'.$datos['cargo'].'</td>';
            echo '<td>'.$datos['cedula'].'</td>';
            echo '</tr>';
        }
    ?>
  </tbody>
</table>