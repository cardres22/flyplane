<?php
 session_start();
 if(isset($_SESSION['datos_usuario'])){
    $dat = $_SESSION['datos_usuario'];
    if($dat['cargo']!="MASTER"){
        header('location: ../../index.php');
    }
}
else{
  header('location: ../../index.php');
}
?>

<form class="needs-validation text-white pb-2 pt-2" id="formato">
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom01">Nombres</label>
            <input type="text" class="form-control" name="nombre" id="validationCustom01" value="Marco" required>
            </div>
            <div class="col-md-6 mb-3">
            <label for="validationCustom02">Apellidos</label>
            <input type="text" class="form-control" name="apellido" id="validationCustom02" value="Polo" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom03">Ciudad</label>
            <input type="text" class="form-control" name="ciudad" id="validationCustom03" required>
            </div>
            <div class="col-md-3 mb-3">
            <label for="validationCustom04">Cargo</label>
            <select class="custom-select" name="cargo" id="validationCustom04" required>
                <option selected disabled value="">Selecciona</option>
                <option>ADMINISTRATIVO</option>
                <option>MASTER</option>
            </select>
            </div>
            <div class="col-md-3 mb-3">
            <label for="validationCustom05">Cédula</label>
            <input type="number" class="form-control" name="cedula" id="validationCustom05" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
            <label for="validationCustom01">Contraseña</label>
            <input type="password" class="form-control" name="password" id="validationCustom01" required>
            </div>
            <div class="col-md-6 mb-3">
            <label for="validationCustom02">Escriba de nuevo su contraseña</label>
            <input type="password" class="form-control" name="password_2" id="validationCustom02"  required>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Registrar</button>
    </form>
    <div class="mt-3" id="alertar">
            
    </div>

    <script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="../../assets/js/add_admin.js"></script>