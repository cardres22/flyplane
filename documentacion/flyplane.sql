-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-07-2020 a las 05:07:37
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `flyplane`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `codigo` varchar(10) NOT NULL,
  `origen` varchar(20) NOT NULL,
  `destino` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`codigo`, `origen`, `destino`) VALUES
('12345', 'LETICIA', 'PEREIRA'),
('15247', 'PASTO', 'CALI'),
('45874', 'MEDELLIN', 'POPAYAN'),
('74584', 'CALI', 'PASTO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `cargo` varchar(20) NOT NULL,
  `cedula` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`nombre`, `apellido`, `ciudad`, `cargo`, `cedula`, `password`) VALUES
('Marco', 'Polo', 'pasto', 'PILOTO', '1085000001', '$2y$10$20DJ4mRbkumFL/c1cls3Qe9qFK.Y85mjtKA.gtN9hmHmfbI4DdAEq'),
('Felipe', 'Polo', 'pasto', 'PILOTO', '1085000002', '$2y$10$y3l3Qb4g50zCvk74P.SW6uvueVM/yWRcZ7sdJOZYcPl3IYYzypbAW'),
('Luis', 'Polo', 'pasto', 'ASISTENTE DE VUELO', '1085000003', '$2y$10$g0wLMVFRykXzUAc0joKhieXflyEhprSBSnNa/GSKnbPmpiAxCuj5m'),
('Mario', 'Polo', 'pasto', 'ASISTENTE DE VUELO', '1085000004', '$2y$10$v0.DXpFzlze1DrtOtFXwx.oOstmHGZ1cQShZaoUaM8nC412bwz7kq'),
('Luisa', 'Lopez', 'Pasto', 'ASISTENTE DE VUELO', '1085000010', '$2y$10$.gILxYClOD/vKjbg5W4ts..8rVWjDbsdstotvBvY4odFIITFk.pN2'),
('Fernanda', 'Martinez', 'Pasto', 'ASISTENTE DE VUELO', '1085000011', '$2y$10$1X6Xbx2bzSa/T8rxZnYlaujMflxwXP603Bk94qvWnn3Ak7tfpzrzG'),
('Martha', 'Muñoz', 'Pasto', 'PILOTO', '1085000012', '$2y$10$pi6PVIWrmnzo4O.A12FRiOb3UTscH.ftQ3dlPKW8FCUICZ.rumq/W'),
('Carlos', 'Guzman', 'Medellin', 'ADMINISTRATIVO', '1085329373', '$2y$10$oqaH2UTM.54GPGtnHhJvSueobPROw5HL1dBTPISD/UHLgHjvsYGQG'),
('Maria', 'Lopez', 'Pasto', 'ADMINISTRATIVO', '1085624745', '$2y$10$kCZN5FEN2iHxY3cGtuCsmO6JisEEEqxJLVGzQtZq44wjGTP27TDKC'),
('Armando', 'Ruiz', 'Cali', 'PILOTO', '123456789', '$2y$10$gIXmuDHTzT4qudE7E0Zmh.d/JGKOk87Et2loUydjQbCFxpP7ToKRW'),
('MASTER', 'ADMIN', 'FLYPLANE', 'MASTER', '998877665544', '$2y$10$c6EWrvcI8uTaf3e5wXoWJOGIAaVwSfxwi4sxdyyc9l4bMvuloMP2C'),
('Roberto', 'Nuñez', 'Madrid', 'ASISTENTE DE VUELO', '999888123', '$2y$10$Uz65kuaFmfmBp.xIQ.dhDedYtjM2TnQwbOK0GtBhYk61BSe2lXtQC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelos`
--

CREATE TABLE `vuelos` (
  `cod_vue` varchar(10) NOT NULL,
  `ruta` varchar(10) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `ced_p1` varchar(15) NOT NULL,
  `nom_p1` varchar(40) NOT NULL,
  `ced_p2` varchar(15) NOT NULL,
  `nom_p2` varchar(40) NOT NULL,
  `ced_as1` varchar(15) NOT NULL,
  `nom_as1` varchar(40) NOT NULL,
  `ced_as2` varchar(15) NOT NULL,
  `nom_as2` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vuelos`
--

INSERT INTO `vuelos` (`cod_vue`, `ruta`, `fecha`, `ced_p1`, `nom_p1`, `ced_p2`, `nom_p2`, `ced_as1`, `nom_as1`, `ced_as2`, `nom_as2`) VALUES
('0001', '45874', '2020-07-02', '1085000001', 'Marco Polo', '1085000002', 'Felipe Polo', '1085000003', 'Luis Polo', '1085000004', 'Mario Polo'),
('0002', '15247', '2020-07-22', '1085000012', 'Martha Muñoz', '1085000001', 'Marco Polo', '1085000010', 'Luisa Lopez', '1085000003', 'Luis Polo'),
('0003', '74584', '2020-07-20', '1085000012', 'Martha Muñoz', '1085000002', 'Felipe Polo', '1085000004', 'Mario Polo', '1085000003', 'Luis Polo'),
('0006', '15247', '2020-07-21', '123456789', 'Armando Ruiz', '1085000001', 'Marco Polo', '1085000003', 'Luis Polo', '1085000011', 'Fernanda Martinez'),
('2000', '12345', '2020-08-01', '123456789', 'Armando Ruiz', '1085000012', 'Martha Muñoz', '1085000003', 'Luis Polo', '1085000010', 'Luisa Lopez');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `vuelos`
--
ALTER TABLE `vuelos`
  ADD PRIMARY KEY (`cod_vue`),
  ADD KEY `ruta` (`ruta`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `vuelos`
--
ALTER TABLE `vuelos`
  ADD CONSTRAINT `vuelos_ibfk_1` FOREIGN KEY (`ruta`) REFERENCES `rutas` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
